//Monetary variables stored in American cents
//Start up function for base settings
function StartUp()
{
	//Base Values
	CurrentYear.innerHTML = 1900;
	CurrentWeek.innerHTML = 1;
	CurrentBalance.innerHTML = 0;
	NewBusinessTotal.innerHTML = 1;
	NewBusinessCost.innerHTML = 10000;

	//Upgrades
	Upgrade_1_Cost.innerHTML = 200;
	Upgrade_2_Cost.innerHTML = 2000;
	Upgrade_3_Cost.innerHTML = 20000;
	Upgrade_4_Cost.innerHTML = 200000;
	Upgrade_5_Cost.innerHTML = 2000000;

	Upgrade_1_Percentage.innerHTML = 0;
	Upgrade_2_Percentage.innerHTML = 0;
	Upgrade_3_Percentage.innerHTML = 0;
	Upgrade_4_Percentage.innerHTML = 0;
	Upgrade_5_Percentage.innerHTML = 0;

	Upgrade_1_Owned.innerHTML = 0;
	Upgrade_2_Owned.innerHTML = 0;
	Upgrade_3_Owned.innerHTML = 0;
	Upgrade_4_Owned.innerHTML = 0;
	Upgrade_5_Owned.innerHTML = 0;
	
}

function LoadPlayerData()
	{
		//Make RawData grab info from a hidden HTML element with the savedata from php
		var RawData = savetransfer.innerHTML;
		//Parsing such data and making it readable to javascript
		var LoadData = JSON.parse(RawData);
		LoadData = JSON.parse(LoadData);
		//Returning data to be used further
		return LoadData;
	}

function LoadBaseVariables(Data){
	var LoadData = Data;
	//Base Values
	CurrentYear.innerHTML = LoadData.year;
	CurrentWeek.innerHTML = LoadData.week;
	CurrentBalance.innerHTML = LoadData.balance/100;
	NewBusinessTotal.innerHTML = LoadData.businesstypesowned;
	NewBusinessCost.innerHTML = LoadData.businesscost/100;
	
	//Businesses
	

	//Upgrades
	Upgrade_1_Cost.innerHTML = LoadData.business1upgradecost/100;
	Upgrade_2_Cost.innerHTML = LoadData.business2upgradecost/100;
	Upgrade_3_Cost.innerHTML = LoadData.business3upgradecost/100;
	Upgrade_4_Cost.innerHTML = LoadData.business4upgradecost/100;
	Upgrade_5_Cost.innerHTML = LoadData.business5upgradecost/100;

	Upgrade_1_Percentage.innerHTML = LoadData.business1upgradepercentage;
	Upgrade_2_Percentage.innerHTML = LoadData.business2upgradepercentage;
	Upgrade_3_Percentage.innerHTML = LoadData.business3upgradepercentage;
	Upgrade_4_Percentage.innerHTML = LoadData.business4upgradepercentage;
	Upgrade_5_Percentage.innerHTML = LoadData.business5upgradepercentage;

	Upgrade_1_Owned.innerHTML = LoadData.business1upgradesowned;
	Upgrade_2_Owned.innerHTML = LoadData.business2upgradesowned;
	Upgrade_3_Owned.innerHTML = LoadData.business3upgradesowned;
	Upgrade_4_Owned.innerHTML = LoadData.business4upgradesowned;
	Upgrade_5_Owned.innerHTML = LoadData.business5upgradesowned;
}



function BusinessModel()
{
	//Variables

	//Account
	USERNAME = sessionStorage.getItem("user");
	AccountTab.innerHTML = USERNAME;

	//General
	var Week = parseInt(CurrentWeek.innerHTML);
	var Year = parseInt(CurrentYear.innerHTML);
	var Balance = parseFloat(CurrentBalance.innerHTML*100);
	var BusinessTypesOwned = parseFloat(NewBusinessTotal.innerHTML);
	var BusinessCost = parseFloat(NewBusinessCost.innerHTML*100);
	var BusinessList = [];

	//Upgrades
	var UpgradeCost1 = parseFloat(Upgrade_1_Cost.innerHTML*100);
	var UpgradeCost2 = parseFloat(Upgrade_2_Cost.innerHTML*100);
	var UpgradeCost3 = parseFloat(Upgrade_3_Cost.innerHTML*100);
	var UpgradeCost4 = parseFloat(Upgrade_4_Cost.innerHTML*100);
	var UpgradeCost5 = parseFloat(Upgrade_5_Cost.innerHTML*100);

	var UpgradePercentage1 = parseFloat(Upgrade_1_Percentage.innerHTML);
	var UpgradePercentage2 = parseFloat(Upgrade_2_Percentage.innerHTML);
	var UpgradePercentage3 = parseFloat(Upgrade_3_Percentage.innerHTML);
	var UpgradePercentage4 = parseFloat(Upgrade_4_Percentage.innerHTML);
	var UpgradePercentage5 = parseFloat(Upgrade_5_Percentage.innerHTML);

	var UpgradesOwned1 = parseFloat(Upgrade_1_Owned.innerHTML);
	var UpgradesOwned2 = parseFloat(Upgrade_2_Owned.innerHTML);
	var UpgradesOwned3 = parseFloat(Upgrade_3_Owned.innerHTML);
	var UpgradesOwned4 = parseFloat(Upgrade_4_Owned.innerHTML);
	var UpgradesOwned5 = parseFloat(Upgrade_5_Owned.innerHTML);

	//End simulation
	EndGame = document.getElementById("EndGamePopup");

	//Stock Market
	var StockTableVisible = false;

	//Event Variables
	//1 in 15 chance
	var EventChance = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
	// 1 or 3 in 50 chance
	var MoneyEventChance = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,2,3];
	var ListOfEvents = ["b",1,1.25,"Premium restaurant customers","b",1,0.5,"Kitchen nightmare!","b",2,1.25,"Factory morale incresed","b",2,0.5,"Factory strike!","b",3,1.25,"New hit film released","b",3,0.5,"Cinematic failure!","b",4,1.25,"Car boom","b",4,0.5,"Car factory strike","b",5,1.25,"VIP Guest","b",5,0.5,"Hotel season over","s",1,"+","Silver needed for new tech","s",1,"-","Silver devaluation","s",2,"+","Oil usage increased","s",2,"-","Oil crisis","s",3,"+","Gold quality improved","s",3,"-","Gold devaluation"];
	var EventList = [];

	//Modifiers
	var RestaurantModifier = 1;
	var FactoryModifier = 1;
	var CinemaModifier = 1;
	var AutomobileModifier = 1;
	var HotelModifier = 1;

	//
	if (BusinessCost == 0)
	{
		document.getElementById("NewBusiness").style.display = "none";
	}

	//Loading week count variable if available else setting it to 0
	if (LoadType == "LOAD")
	{
		var LoadData = LoadPlayerData();
		var WeekCount = LoadData.weekcount;
	}
	else
	{
		var WeekCount = 0;
	}

	//Makes 2D array for events
	function MakeEventList(EventList,ListOfEvents)
	{
		var EventInpCounter = 0;

		for (i = 0; i < ListOfEvents.length/4; i++)
		{
			EventList[i]=[];
			for (j = 0; j < 4; j++)
			{
				EventList[i][j] = ListOfEvents[EventInpCounter];
				EventInpCounter += 1;

			}
		}
	}
	MakeEventList(EventList,ListOfEvents);

	//Saving data functions before sending data to server
	//Regular save
	document.getElementById("SaveForm").onsubmit = function(){SaveGame()};

	//End game save
	document.getElementById("ModalSaveForm").onsubmit = function(){ModalSaveGame()};
	
	
	//Buttons
		

	//Weeks
	NextWeek = document.getElementById("NextWeek");

	//Businesses
	BUYBUSINESS = document.getElementById("NewBusiness");

	//Upgrades
	UPGRADE_1 = document.getElementById("Upgrade_1");
	UPGRADE_2 = document.getElementById("Upgrade_2");
	UPGRADE_3 = document.getElementById("Upgrade_3");
	UPGRADE_4 = document.getElementById("Upgrade_4");
	UPGRADE_5 = document.getElementById("Upgrade_5");

	//Stock market
	TOGGLESTOCKTABLE = document.getElementById("ToggleStockTable_btn");
	STOCKBUY_1 = document.getElementById("StockBuy1_btn");
	STOCKBUY_2 = document.getElementById("StockBuy2_btn");
	STOCKBUY_3 = document.getElementById("StockBuy3_btn");
	STOCKSELL_1 = document.getElementById("StockSell1_btn");
	STOCKSELL_2 = document.getElementById("StockSell2_btn");
	STOCKSELL_3 = document.getElementById("StockSell3_btn");



	//End game trigger (in case of refresh or load) - $1 billion needed
	if (Balance >= 100000000000)
	{
		EndSimulation();
	}
	else
	{

	}


	function StockMarketStartUp()
	{
		//Setting up html values in stock market table
		StockBuy1_val.innerHTML = Stock_1.ShareBuyPrice/100;
		StockBuy2_val.innerHTML = Stock_2.ShareBuyPrice/100;
		StockBuy3_val.innerHTML = Stock_3.ShareBuyPrice/100;
		StockSell1_val.innerHTML = Stock_1.ShareSellPrice/100;
		StockSell2_val.innerHTML = Stock_2.ShareSellPrice/100;
		StockSell3_val.innerHTML = Stock_3.ShareSellPrice/100;
		StockShares1.innerHTML = Stock_1.SharesOwned;
		StockShares2.innerHTML = Stock_2.SharesOwned;
		StockShares3.innerHTML = Stock_3.SharesOwned;
		StockValue1.innerHTML = Stock_1.SharesVal/100;
		StockValue2.innerHTML = Stock_2.SharesVal/100;
		StockValue3.innerHTML =	Stock_3.SharesVal/100;
	}
	
	function SaveGame()
	{
	//Save data form values
	document.getElementById("SaveDataField").value = SavePlayerData();
	document.getElementById("SaveDataUser").value = USERNAME;
	}

	//End game save game
	function ModalSaveGame()
	{
	//Save data form values
	document.getElementById("ModalSaveDataField").value = SavePlayerData();
	document.getElementById("ModalSaveDataUser").value = USERNAME;
	document.getElementById("FinalUserScore").value = WeekCount;
	}

	function SavePlayerData ()
	{	
		//Saving data to be sent to server in JSON
		var SaveData = {year: Year, week: Week, balance: Balance, businesstypesowned: BusinessTypesOwned, businesscost: BusinessCost, business1type: restaurant.BusinessType, business1properties: restaurant.Properties, business1income: restaurant.Income, business1upgradepercentage: restaurant.UpgradePercentage, business1upgradesowned: restaurant.UpgradesOwned, business1upgradecost: restaurant.UpgradeCost, business1businesscost: restaurant.BusinessCost, business2type: factory.BusinessType, business2properties: factory.Properties, business2income: factory.Income, business2upgradepercentage: factory.UpgradePercentage, business2upgradesowned: factory.UpgradesOwned, business2upgradecost: factory.UpgradeCost, business2businesscost: factory.BusinessCost, business3type: cinema.BusinessType, business3properties: cinema.Properties, business3income: cinema.Income, business3upgradepercentage: cinema.UpgradePercentage, business3upgradesowned: cinema.UpgradesOwned, business3upgradecost: cinema.UpgradeCost, business3businesscost: cinema.BusinessCost, business4type: automobile.BusinessType, business4properties: automobile.Properties, business4income: automobile.Income, business4upgradepercentage: automobile.UpgradePercentage, business4upgradesowned: automobile.UpgradesOwned, business4upgradecost: automobile.UpgradeCost, business4businesscost: automobile.BusinessCost, business5type: hotel.BusinessType, business5properties: hotel.Properties, business5income: hotel.Income, business5upgradepercentage: hotel.UpgradePercentage, business5upgradesowned: hotel.UpgradesOwned, business5upgradecost: hotel.UpgradeCost, business5businesscost: hotel.BusinessCost, stock1companycode: stock1.CompanyCode, stock1stockbuyprice: stock1.ShareBuyPrice, stock1stocksellprice: stock1.ShareSellPrice, stock1change: stock1.Change, stock1skew: stock1.Skew, stock1sharesowned: stock1.SharesOwned, stock1sharesval: stock1.SharesVal, stock2companycode: stock2.CompanyCode, stock2stockbuyprice: stock2.ShareBuyPrice, stock2stocksellprice: stock2.ShareSellPrice, stock2change: stock2.Change, stock2skew: stock2.Skew, stock2sharesowned: stock2.SharesOwned, stock2sharesval: stock2.SharesVal, stock3companycode: stock3.CompanyCode, stock3stockbuyprice: stock3.ShareBuyPrice, stock3stocksellprice: stock3.ShareSellPrice, stock3change: stock3.Change, stock3skew: stock3.Skew, stock3sharesowned: stock3.SharesOwned, stock3sharesval: stock3.SharesVal, weekcount: WeekCount}
		var SendData = JSON.stringify(SaveData);
		return SendData;

	}

	function EndSimulation ()
	{
		//End game pop up display turned on
		EndGame.style.display = "block";
		//Showing weeks taken to finish simulation
		WeeksTaken.innerHTML = WeekCount;
	}

	
	//Function calculating each weeks income and events
	function NextWeekCalc (EventChance,MoneyEventChance,EventList,RestaurantModifier,FactoryModifier,CinemaModifier,AutomobileModifier,HotelModifier)
	{
		//Gift variable that can be obtained through the event system
		var gift = 0;

		//End game trigger (when reached in game) - $1 billion needed
		if (Balance >= 100000000000)
		{
			EndSimulation();
		}
		else
		{

		}

		//Date Update
		Week += 1;
		
		if (Week > 52)
		{
			Year += 1;
			Week = 1;
		}

		//Event Update
		if (Year >= 1920)
		{
			//Randomising chance at monetary gift
			var MoneyEventChanceIndex = Math.floor(Math.random(0) * MoneyEventChance.length);
			var MoneyEventChanceRand = MoneyEventChance[MoneyEventChanceIndex];

			//Randomising chance at event (after 1920)
			var EventChanceIndex = Math.floor(Math.random(0) * EventChance.length);
			var EventChanceRand = EventChance[EventChanceIndex];

			//Monetary gift check
			if (MoneyEventChanceRand == 1)
			{
				gift += 1000000;
				EventTableUpdate("Sponsor gift received");
			}
			else if (MoneyEventChanceRand == 2)
			{
				gift += 5000000;
				EventTableUpdate("Offshore investment received");
			}
			else if (MoneyEventChanceRand == 3)
			{
				gift -= 5000000
				EventTableUpdate("Some of our assets were seized!")
			}
			//Event check
			else if (EventChanceRand == 1)
			{
				var EventTypeIndex = Math.floor(Math.random(0) * EventList.length);
				var RandEvent = EventList[EventTypeIndex];

				//Business related events (property multiplier activated that week)
				if (RandEvent[0] == "b")
				{
					if (RandEvent[1] == 1)
					{
						RestaurantModifier = RandEvent[2];
						EventTableUpdate(RandEvent[3]);
					}
					else if (RandEvent[1] == 2)
					{
						FactoryModifier = RandEvent[2];
						EventTableUpdate(RandEvent[3]);
					}
					else if (RandEvent[1] == 3)
					{
						CinemaModifier = RandEvent[2];
						EventTableUpdate(RandEvent[3]);
					}
					else if (RandEvent[1] == 4)
					{
						AutomobileModifier = RandEvent[2];
						EventTableUpdate(RandEvent[3]);
					}
					else if (RandEvent[1] == 5)
					{
						HotelModifier = RandEvent[2];
						EventTableUpdate(RandEvent[3]);
					}
					else
					{

					}
				} 
				//Stock market events (changes skew of market either up or down)
				else
				{
					if (RandEvent[1] == 1)
					{
						
						if (RandEvent[2] == "+")
						{
							Updated = stock1.UpdateSkew(1);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							}
						}
						else
						{
							Updated = stock1.UpdateSkew(0);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							}
						}
					}
					else if (RandEvent[1] == 2)
					{
						if (RandEvent[2] == "+")
						{
							Updated = stock2.UpdateSkew(1);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							}
						}
						else
						{
							Updated = stock2.UpdateSkew(0);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							}
						}
					}
					else if (RandEvent[1] == 3)
					{
						if (RandEvent[2] == "+")
						{
							Updated = stock3.UpdateSkew(1);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							}
						}
						else
						{
							Updated = stock3.UpdateSkew(0);
							if (Updated == true)
							{
								EventTableUpdate(RandEvent[3]);
							}
							else
							{

							} 
						}
					}
					else
					{

					}
					
				}
			}
			else
			{

			}
		}
		

		//Balance Update - with multipliers (set to 1 if no business event triggered)
		var Earnings = 0;
		Earnings += restaurant.generateIncome() * RestaurantModifier;
		Earnings += factory.generateIncome() * FactoryModifier;
		Earnings += cinema.generateIncome() * CinemaModifier;
		Earnings += automobile.generateIncome() * AutomobileModifier;
		Earnings += hotel.generateIncome() * HotelModifier;
		Earnings += gift;
		
		//Adding earnings to balance
		Balance += Earnings;

		//Updating HTML
		CurrentBalance.innerHTML = Balance/100;
		CurrentYear.innerHTML = Year;
		CurrentWeek.innerHTML = Week;

		//Redirecting to Stock Market Function
		StockMarketCalc();
	}

	//Function that updates the event table on screen and displays what event has been triggered
	function EventTableUpdate(EventText) 
	{
	  var Table = document.getElementById("EventTable");
	  var EventRow = Table.insertRow(0);
	  var Event = EventRow.insertCell(0);
	  Event.innerHTML = EventText;
	  document.getElementById("EventTable").deleteRow(-1);
	}

	//Function regarding stock market changes each week
	function StockMarketCalc()
	{
		//Stock Market change
		//Stock buy price change - variables
		var StockBuyChange1 = stock1.ChangePrice();
		var StockBuyChange2 = stock2.ChangePrice();
		var StockBuyChange3 = stock3.ChangePrice();
		//Stock sell price change - variables
		var StockSellChange1 = stock1.ChangeSell();
		var StockSellChange2 = stock2.ChangeSell();
		var StockSellChange3 = stock3.ChangeSell();
		//Stock buy price change - in HTML
		StockBuy1_val.innerHTML = StockBuyChange1/100;
		StockBuy2_val.innerHTML = StockBuyChange2/100;
		StockBuy3_val.innerHTML = StockBuyChange3/100;
		//Stock sell price change - in HTML
		StockSell1_val.innerHTML = StockSellChange1/100;
		StockSell2_val.innerHTML = StockSellChange2/100;
		StockSell3_val.innerHTML = StockSellChange3/100;
		//Stock value update - in HTML
		StockValue1.innerHTML = stock1.SharesValUpdate();
		StockValue2.innerHTML = stock2.SharesValUpdate();
		StockValue3.innerHTML = stock3.SharesValUpdate();
	}

	//Parent class for all businesses 
	class BusinessChild {

		constructor(BusinessType, Properties, Income, UpgradePercentage, UpgradesOwned, UpgradeCost, BusinessCost)
		{
			this.BusinessType = BusinessType;
			this.Properties = Properties;
			this.Income = Income;
			this.UpgradePercentage = UpgradePercentage;
			this.UpgradesOwned = UpgradesOwned;
			this.UpgradeCost = UpgradeCost;
			this.BusinessCost = BusinessCost;
		}
		//General information about objects
		getInfo(BusinessType, Properties, Income, UpgradePercentage, UpgradesOwned, UpgradeCost) 
		{
			return "Business Type: " + this.BusinessType + "  " + "Number of properties: " + this.Properties + 
			"  " + "Income in cents: " + this.Income + "  " + "Upgrade Bonus: " + this.UpgradePercentage + "%  " + 
			"Upgrades Owned: " + this.UpgradesOwned + " " + "Next Upgrade in cents:" + UpgradeCost + " " + "Business Cost in cents: " + BusinessCost;
		}
		//Getting amount of properties
		getProperty(Properties)
		{
			this.Properties = 1;
		}
		//Getting business cost
		getCost(BusinessCost)
		{
			return this.BusinessCost;
		}
		//Generating business income based on base income, upgrades and properties owned
		generateIncome(Properties, Income, UpgradePercentage) 
		{
			UpgradePercentage = 100 + this.UpgradePercentage;
			Income = this.Properties * this.Income * (UpgradePercentage/100);
			
			return Income;
		}
		//Increasing update bonus once bought
		increaseBonus(UpgradePercentage)
		{
			UpgradePercentage = this.UpgradePercentage + 10;
			this.UpgradePercentage = UpgradePercentage;
			return UpgradePercentage;
		}
		//Increasing upgrades owned counter
		increaseUO(UpgradesOwned)
		{
			UpgradesOwned = this.UpgradesOwned + 1;
			this.UpgradesOwned = UpgradesOwned; 
			return UpgradesOwned;
		}
		//Calculating cost for next upgrade - exponential
		nextUpgrade(UpgradeCost, UpgradesOwned)
		{
			UpgradeCost = Math.round((this.UpgradeCost * Math.pow(2,this.UpgradesOwned))*100)/100;
			this.UpgradeCost = UpgradeCost;
			return UpgradeCost;
		}

	};

	//Stock company class
	class StockCompClass {
		
		//Class constructor
		constructor(CompanyCode, ShareBuyPrice, ShareSellPrice, Change, Skew, SharesOwned, SharesVal)
		{
			this.CompanyCode = CompanyCode;
			this.ShareBuyPrice = ShareBuyPrice;
			this.ShareSellPrice = ShareSellPrice;
			this.Change = Change;
			this.Skew = Skew;
			this.SharesOwned = SharesOwned;
			this.SharesVal = SharesVal;
			

		}
		
		//General information about each stock
		getInfo(CompanyCode, ShareBuyPrice, ShareSellPrice, Change, Skew, SharesOwned, SharesVal) 
		{
			return "Company Name: " + this.CompanyCode + "  " + "Buy Price: $" + this.ShareBuyPrice + "  " + "Sell Price: $" + this.ShareSellPrice + "  " + "Percentage Change: " + this.Change + "  " + "Skew: " + this.Skew + " " + "Shares Owned: " + this.SharesOwned + "  " + "Shares Value: $" + this.SharesVal;
		}

		//Updates balance and stocks owned when stocks bought
		BuyPriceUpdate(ShareBuyPrice,SharesOwned)
		{
			if (Balance >= this.ShareBuyPrice)
			{
				Balance = Balance - this.ShareBuyPrice;
				this.SharesOwned += 1;
			}
			else
			{
				alert("Insufficient Funds!");
			}
			CurrentBalance.innerHTML = Balance/100;
			return this.SharesOwned;

			
		}

		//Updates shares value in stock table
		SharesValUpdate(ShareSellPrice,SharesOwned,SharesVal)
		{
			this.SharesVal = this.SharesOwned * this.ShareSellPrice;
			return this.SharesVal/100;
		}
		
		//Updates balance and stocks owned when stock sold
		SellPriceUpdate(ShareBuyPrice,ShareSellPrice,SharesOwned)
		{
			
			if (this.SharesOwned > 0)
			{
				var profit = this.ShareSellPrice;
				Balance += profit;
				this.SharesOwned -= 1;
			}
			else
			{
				alert("You do not own any shares!")
			}
			CurrentBalance.innerHTML = Balance/100;
			return this.SharesOwned;
			
		}
		
		//Updates stock prices
		ChangePrice(ShareBuyPrice,Skew){

			//Stock variables
			var ShareBuyPrice = this.ShareBuyPrice;
			var Skew = this.Skew;
			var Self = this;
			var SkewIndex = Math.floor(Math.random(0) * Skew.length);
			var SkewDir = Skew[SkewIndex];				
			var Updated = Self.UpdateSkew(SkewDir);
			var RandomNum = 0;

			//Checking for skew update
			if (Updated == true)
			{
				if (SkewDir == 0)
				{
					RandomNum =  -1 * (Math.floor(Math.random(1) * 4));
				}
				else
				{
					RandomNum = Math.floor(Math.random(1) * 5);
				}
			}
			else
			{

			}

			//Stock calculation
			var NewPrice = ShareBuyPrice * (1 + (RandomNum/100))

			//Updating stock price in object
			this.ShareBuyPrice = NewPrice;
			return NewPrice;			
			
		}
		//Updating stock sell price - same as buy price
		ChangeSell (ShareBuyPrice,ShareSellPrice){
			var NewSell = this.ShareBuyPrice;
			this.ShareSellPrice = NewSell;
			return NewSell;
		
		}

		UpdateSkew(Direction,Skew)
		{
			//Direction specifies if changes to stock are positive or negative, 1 or 0
			var Direction = Direction;
			var Skew = this.Skew;
			var LengthZero = 0;
			var LengthOne = 0;
			var Updated = false;

			//Counting the 1's and 0's in Skew
			for (i = 0; i < Skew.length; i++)
			{
				if (Skew[i] == 0)
				{
					LengthZero += 1;
				}
				else
				{
					LengthOne += 1;
				}
			}

			//Algorithm for skew change
			if (Direction == 0)
			{
				if (LengthOne > 1)
				{
					Skew.pop();
					Updated = true;
				}
				else
				{
					if (LengthZero < 3)
					{
						Skew.unshift(0);
						Updated = true;
					}
					else
					{

					}
				}
			}
			else
			{
				if (LengthZero > 1)
				{
					Skew.shift();
					Updated = true;
				}
				else
				{
					if (LengthOne < 5)
					{
						Skew.push(1);
						Updated = true;
					}
					else
					{

					}
				}
			}

			this.Skew = Skew;
			return Updated;

		}		
	};
	
	//Business objects - not in class
	var Business = 
	{

		objectInfo: function(BusinessType, Properties, Income, UpgradePercentage, UpgradesOwned, UpgradeCost) 
		{
			return "Business Type: " + this.BusinessType + "  " + "Number of properties: " + this.Properties + 
			"  " + "Income: $" + this.Income + "  " + "Upgrade Bonus: " + this.UpgradePercentage + "%  " + 
			"Upgrades Owned: " + this.UpgradesOwned + " " + "Next upgrade $" + this.UpgradeCost + " " + " Business Cost: $" + this.BusinessCost;
		}
	}

	//Stock company objects - not in class
	var StockCompany = 
	{
		
		objectInfo: function(CompanyCode, ShareBuyPrice, ShareSellPrice, Change, Skew, SharesOwned, SharesVal) {
			return "Company Name: " + this.CompanyCode + "  " + "Buy Price: $" + this.ShareBuyPrice + "  " + "Sell Price: $" + this.ShareSellPrice + "  " + "Percentage Change: " + this.Change + "  " + "Shares Owned: " + this.SharesOwned + "  " + "Shares Value: $" + this.SharesVal;
		}
	}

	//Instances of objects
	
	//BUSINESSES

	//Loading objects - if load game is initialised
	if (LoadType == "LOAD")
	{	
		DATA = LoadPlayerData();
		//Restaurant business creation
		var  Restaurant = Object.create(Business);
		Restaurant.BusinessType = DATA.business1type;
		Restaurant.Properties = DATA.business1properties;
		Restaurant.Income = DATA.business1income;
		Restaurant.UpgradePercentage = DATA.business1upgradepercentage;
		Restaurant.UpgradesOwned = DATA.business1upgradesowned;
		Restaurant.UpgradeCost = DATA.business1upgradecost;
		Restaurant.BusinessCost = DATA.business1businesscost;
		BusinessList.push(Restaurant.BusinessType);
		//Creating object from BusinessChild class
		restaurant = new BusinessChild(Restaurant.BusinessType,Restaurant.Properties,Restaurant.Income,Restaurant.UpgradePercentage,Restaurant.UpgradesOwned,Restaurant.UpgradeCost,Restaurant.BusinessCost);

		//Factory business creation
		var Factory = Object.create(Business);
		Factory.BusinessType = DATA.business2type;
		Factory.Properties = DATA.business2properties;
		Factory.Income = DATA.business2income;
		Factory.UpgradePercentage = DATA.business2upgradepercentage;
		Factory.UpgradesOwned = DATA.business2upgradesowned;
		Factory.UpgradeCost = DATA.business2upgradecost;
		Factory.BusinessCost = DATA.business2businesscost;
		BusinessList.push(Factory.BusinessType);
		//Creating object from BusinessChild class
		factory = new BusinessChild(Factory.BusinessType,Factory.Properties,Factory.Income,Factory.UpgradePercentage,Factory.UpgradesOwned,Factory.UpgradeCost,Factory.BusinessCost);
	
		//Cinema business creation
		var Cinema = Object.create(Business);
		Cinema.BusinessType = DATA.business3type;
		Cinema.Properties = DATA.business3properties;
		Cinema.Income = DATA.business3income;
		Cinema.UpgradePercentage = DATA.business3upgradepercentage;
		Cinema.UpgradesOwned = DATA.business3upgradesowned;
		Cinema.UpgradeCost = DATA.business3upgradecost;
		Cinema.BusinessCost = DATA.business3businesscost;
		BusinessList.push(Cinema.BusinessType);
		//Creating object from BusinessChild class
		cinema = new BusinessChild(Cinema.BusinessType,Cinema.Properties,Cinema.Income,Cinema.UpgradePercentage,Cinema.UpgradesOwned,Cinema.UpgradeCost,Cinema.BusinessCost);
		
		//Automobile business creation
		var Automobile = Object.create(Business);
		Automobile.BusinessType = DATA.business4type;
		Automobile.Properties = DATA.business4properties;
		Automobile.Income = DATA.business4income;
		Automobile.UpgradePercentage = DATA.business4upgradepercentage;
		Automobile.UpgradesOwned = DATA.business4upgradesowned;
		Automobile.UpgradeCost = DATA.business4upgradecost;
		Automobile.BusinessCost = DATA.business4businesscost;
		BusinessList.push(Automobile.BusinessType);
		//Creating object from BusinessChild class
		automobile = new BusinessChild(Automobile.BusinessType,Automobile.Properties,Automobile.Income,Automobile.UpgradePercentage,Automobile.UpgradesOwned,Automobile.UpgradeCost,Automobile.BusinessCost);
	
		//Hotel business creation
		var Hotel = Object.create(Business);
		Hotel.BusinessType = DATA.business5type;
		Hotel.Properties = DATA.business5properties;
		Hotel.Income = DATA.business5income;
		Hotel.UpgradePercentage = DATA.business5upgradepercentage;
		Hotel.UpgradesOwned = DATA.business5upgradesowned;
		Hotel.UpgradeCost = DATA.business5upgradecost;
		Hotel.BusinessCost = DATA.business5businesscost;
		BusinessList.push(Hotel.BusinessType);
		//Creating object from BusinessChild class
		hotel = new BusinessChild(Hotel.BusinessType,Hotel.Properties,Hotel.Income,Hotel.UpgradePercentage,Hotel.UpgradesOwned,Hotel.UpgradeCost,Hotel.BusinessCost);
		
	}
	else
	{
		//Restaurant business creation
		var  Restaurant = Object.create(Business);
		Restaurant.BusinessType = "Restaurant";
		Restaurant.Properties = 1;
		Restaurant.Income = 3000;
		Restaurant.UpgradePercentage = 0;
		Restaurant.UpgradesOwned = 0;
		Restaurant.UpgradeCost = 20000;
		Restaurant.BusinessCost = 0;
		BusinessList.push(Restaurant.BusinessType);
		//Creating object from BusinessChild class
		restaurant = new BusinessChild(Restaurant.BusinessType,Restaurant.Properties,Restaurant.Income,Restaurant.UpgradePercentage,Restaurant.UpgradesOwned,Restaurant.UpgradeCost,Restaurant.BusinessCost);
	
		//Factory business creation
		var Factory = Object.create(Business);
		Factory.BusinessType = "Factory";
		Factory.Properties = 0;
		Factory.Income = 70000;
		Factory.UpgradePercentage = 0;
		Factory.UpgradesOwned = 0;
		Factory.UpgradeCost = 200000;
		Factory.BusinessCost = 1000000;
		BusinessList.push(Factory.BusinessType);
		//Creating object from BusinessChild class
		factory = new BusinessChild(Factory.BusinessType,Factory.Properties,Factory.Income,Factory.UpgradePercentage,Factory.UpgradesOwned,Factory.UpgradeCost,Factory.BusinessCost);
	
		//Cinema business creation
		var Cinema = Object.create(Business);
		Cinema.BusinessType = "Cinema";
		Cinema.Properties = 0;
		Cinema.Income = 430000;
		Cinema.UpgradePercentage = 0;
		Cinema.UpgradesOwned = 0;
		Cinema.UpgradeCost = 2000000;
		Cinema.BusinessCost = 10000000;
		BusinessList.push(Cinema.BusinessType);
		//Creating object from BusinessChild class
		cinema = new BusinessChild(Cinema.BusinessType,Cinema.Properties,Cinema.Income,Cinema.UpgradePercentage,Cinema.UpgradesOwned,Cinema.UpgradeCost,Cinema.BusinessCost);
	
		//Automobile business creation
		var Automobile = Object.create(Business);
		Automobile.BusinessType = "Automobile";
		Automobile.Properties = 0;
		Automobile.Income = 1000000;
		Automobile.UpgradePercentage = 0;
		Automobile.UpgradesOwned = 0;
		Automobile.UpgradeCost = 20000000;
		Automobile.BusinessCost = 100000000;
		BusinessList.push(Automobile.BusinessType);
		//Creating object from BusinessChild class
		automobile = new BusinessChild(Automobile.BusinessType,Automobile.Properties,Automobile.Income,Automobile.UpgradePercentage,Automobile.UpgradesOwned,Automobile.UpgradeCost,Automobile.BusinessCost);
	
		//Hotel business creation
		var Hotel = Object.create(Business);
		Hotel.BusinessType = "Hotel";
		Hotel.Properties = 0;
		Hotel.Income = 4000000;
		Hotel.UpgradePercentage = 0;
		Hotel.UpgradesOwned = 0;
		Hotel.UpgradeCost = 200000000;
		Hotel.BusinessCost = 1000000000;
		BusinessList.push(Hotel.BusinessType);
		//Creating object from BusinessChild class
		hotel = new BusinessChild(Hotel.BusinessType,Hotel.Properties,Hotel.Income,Hotel.UpgradePercentage,Hotel.UpgradesOwned,Hotel.UpgradeCost,Hotel.BusinessCost);
	}

	//STOCK COMPANIES
	
	//Checking if game is loaded
	if (LoadType == "LOAD")
	{	
		//Load stock objects
		//Stock 1
		DATA = LoadPlayerData();
		var Stock_1 = Object.create(StockCompany);
		Stock_1.CompanyCode = DATA.stock1companycode;
		Stock_1.ShareBuyPrice = DATA.stock1stockbuyprice;
		Stock_1.ShareSellPrice = DATA.stock1stocksellprice;
		Stock_1.Change = DATA.stock1change;
		Stock_1.Skew = DATA.stock1skew;
		Stock_1.SharesOwned = DATA.stock1sharesowned;
		Stock_1.SharesVal = DATA.stock1sharesval;
		//Creating object from StockCompClass class
		stock1 = new StockCompClass(Stock_1.CompanyCode,Stock_1.ShareBuyPrice,Stock_1.ShareSellPrice,Stock_1.Change,Stock_1.Skew,Stock_1.SharesOwned,Stock_1.SharesVal);
		
		//Stock 2
		var Stock_2 = Object.create(StockCompany);
		Stock_2.CompanyCode = DATA.stock2companycode;
		Stock_2.ShareBuyPrice = DATA.stock2stockbuyprice;
		Stock_2.ShareSellPrice = DATA.stock2stocksellprice;
		Stock_2.Change = DATA.stock2change;
		Stock_2.Skew = DATA.stock2skew;
		Stock_2.SharesOwned = DATA.stock2sharesowned;
		Stock_2.SharesVal = DATA.stock2sharesval;
		//Creating object from StockCompClass class
		stock2 = new StockCompClass(Stock_2.CompanyCode,Stock_2.ShareBuyPrice,Stock_2.ShareSellPrice,Stock_2.Change,Stock_2.Skew,Stock_2.SharesOwned,Stock_2.SharesVal);
		
		//Stock 3
		var Stock_3 = Object.create(StockCompany);
		Stock_3.CompanyCode = DATA.stock3companycode;
		Stock_3.ShareBuyPrice = DATA.stock3stockbuyprice;
		Stock_3.ShareSellPrice = DATA.stock3stocksellprice;
		Stock_3.Change = DATA.stock3change;
		Stock_3.Skew = DATA.stock3skew;
		Stock_3.SharesOwned = DATA.stock3sharesowned;
		Stock_3.SharesVal = DATA.stock3sharesval;
		//Creating object from StockCompClass class
		stock3 = new StockCompClass(Stock_3.CompanyCode,Stock_3.ShareBuyPrice,Stock_3.ShareSellPrice,Stock_3.Change,Stock_3.Skew,Stock_3.SharesOwned,Stock_3.SharesVal);
	}
	//New game variables
	else
	{
		//Stock 1
		var Stock_1 = Object.create(StockCompany);
		Stock_1.CompanyCode = "Silver";
		Stock_1.ShareBuyPrice = 700000;
		Stock_1.ShareSellPrice = 700000;
		Stock_1.Change = [3,3];
		Stock_1.Skew = [0,1];
		Stock_1.SharesOwned = 0;
		Stock_1.SharesVal = 0;
		//Creating object from StockCompClass class
		stock1 = new StockCompClass(Stock_1.CompanyCode,Stock_1.ShareBuyPrice,Stock_1.ShareSellPrice,Stock_1.Change,Stock_1.Skew,Stock_1.SharesOwned,Stock_1.SharesVal);
		
		//Stock 2
		var Stock_2 = Object.create(StockCompany);
		Stock_2.CompanyCode = "Oil";
		Stock_2.ShareBuyPrice = 1000000;
		Stock_2.ShareSellPrice = 1000000;
		Stock_2.Change = [3,3];
		Stock_2.Skew = [0,1];
		Stock_2.SharesOwned = 0;
		Stock_2.SharesVal = 0;
		//Creating object from StockCompClass class
		stock2 = new StockCompClass(Stock_2.CompanyCode,Stock_2.ShareBuyPrice,Stock_2.ShareSellPrice,Stock_2.Change,Stock_2.Skew,Stock_2.SharesOwned,Stock_2.SharesVal);
		
		//Stock 3
		var Stock_3 = Object.create(StockCompany);
		Stock_3.CompanyCode = "Gold";
		Stock_3.ShareBuyPrice = 2500000;
		Stock_3.ShareSellPrice = 2500000;
		Stock_3.Change = [3,3];
		Stock_3.Skew = [0,1];
		Stock_3.SharesOwned = 0;
		Stock_3.SharesVal = 0;
		//Creating object from StockCompClass class
		stock3 = new StockCompClass(Stock_3.CompanyCode,Stock_3.ShareBuyPrice,Stock_3.ShareSellPrice,Stock_3.Change,Stock_3.Skew,Stock_3.SharesOwned,Stock_3.SharesVal);
	}

	StockMarketStartUp();

	//Buy New Business button click event
	BUYBUSINESS.onclick = function()
	{

		//Checking which business has been bought (1 - 4 resemble the 4 additional businesses up for sale)
		switch (BusinessTypesOwned)
		{
			case 1:
				Cost = factory.getCost();
				if (Balance >= Cost)
				{
					Balance -= Cost;
					BusinessCost = cinema.getCost();
					factory.getProperty();
					BusinessTypesOwned += 1;

				}
				else
				{
				alert("Not enough funds");
				}
				break;
				
			case 2:
				Cost = cinema.getCost();
				if (Balance >= Cost)
				{
					Balance -= Cost;
					BusinessCost = automobile.getCost();
					cinema.getProperty();
					BusinessTypesOwned += 1;

				}
				else
				{
				alert("Not enough funds");
				}
				break;
			
			case 3:
				Cost = automobile.getCost();
				if (Balance >= Cost)
				{
					Balance -= Cost;
					BusinessCost = hotel.getCost();
					automobile.getProperty();
					BusinessTypesOwned += 1;

				}
				else
				{
				alert("Not enough funds");
				}
				break;
				
			case 4:
				Cost = hotel.getCost();
				if (Balance >= Cost)
				{
					Balance -= Cost;
					BusinessCost = 0;
					hotel.getProperty();
					BusinessTypesOwned += 1;
					document.getElementById("NewBusiness").style.display = "none";

				}
				else
				{
				alert("Not enough funds");
				}
				break;

				
			default:
				alert("You have bought all businesses! There are no more businesses to buy!");

		
		}
		//Updating balance, number of businesses owned and business cost in HTML
		CurrentBalance.innerHTML = Balance/100;
		NewBusinessTotal.innerHTML = BusinessTypesOwned;
		NewBusinessCost.innerHTML = BusinessCost/100;
	};

	//Upgrade button click events - These are the same but link to the 5 different businesses separately
	UPGRADE_1.onclick = function()
	{

		if (Balance >= UpgradeCost1){
			Balance = Balance - UpgradeCost1;

			//Increasing upgrade bonus
			UpgradePercentage1 = restaurant.increaseBonus();
			Restaurant.UpgradePercentage = UpgradePercentage1;
			
			//Increasing upgrades owned counter
			UpgradesOwned1 = restaurant.increaseUO();
			Restaurant.UpgradesOwned = UpgradesOwned1;

			//Calculating next upgrade price
			UpgradeCost1 = restaurant.nextUpgrade();
			Restaurant.UpgradeCost = UpgradeCost1;

			//Updating HTML with these values
			CurrentBalance.innerHTML = Balance/100;
			Upgrade_1_Percentage.innerHTML = UpgradePercentage1;
			Upgrade_1_Owned.innerHTML = UpgradesOwned1;
			Upgrade_1_Cost.innerHTML = UpgradeCost1/100;

			}
			else
			{
				alert("You do not have enough funds!");
			}
	};
		UPGRADE_2.onclick = function()
		{

		if (Balance >= UpgradeCost2){
			Balance = Balance - UpgradeCost2;

			UpgradePercentage2 = factory.increaseBonus();
			Factory.UpgradePercentage = UpgradePercentage2;
			
			UpgradesOwned2 = factory.increaseUO();
			Factory.UpgradesOwned = UpgradesOwned2;

			UpgradeCost2 = factory.nextUpgrade();
			Factory.UpgradeCost = UpgradeCost2;

			CurrentBalance.innerHTML = Balance/100;
			Upgrade_2_Percentage.innerHTML = UpgradePercentage2;
			Upgrade_2_Owned.innerHTML = UpgradesOwned2;
			Upgrade_2_Cost.innerHTML = UpgradeCost2/100;

			}
			else
			{
				alert("You do not have enough funds!");
			}
	};
		UPGRADE_3.onclick = function()
		{

		if (Balance >= UpgradeCost3){
			Balance = Balance - UpgradeCost3;

			UpgradePercentage3 = cinema.increaseBonus();
			Cinema.UpgradePercentage = UpgradePercentage3;
			
			UpgradesOwned3 = cinema.increaseUO();
			Cinema.UpgradesOwned = UpgradesOwned3;

			UpgradeCost3 = cinema.nextUpgrade();
			Cinema.UpgradeCost = UpgradeCost3;

			CurrentBalance.innerHTML = Balance/100;
			Upgrade_3_Percentage.innerHTML = UpgradePercentage3;
			Upgrade_3_Owned.innerHTML = UpgradesOwned3;
			Upgrade_3_Cost.innerHTML = UpgradeCost3/100;

			}
			else
			{
				alert("You do not have enough funds!");
			}
	};
		UPGRADE_4.onclick = function()
		{

		if (Balance >= UpgradeCost4){
			Balance = Balance - UpgradeCost4;

			UpgradePercentage4 = automobile.increaseBonus();
			Automobile.UpgradePercentage = UpgradePercentage4;
			
			UpgradesOwned4 = automobile.increaseUO();
			Automobile.UpgradesOwned = UpgradesOwned4;

			UpgradeCost4 = automobile.nextUpgrade();
			Automobile.UpgradeCost = UpgradeCost4;

			CurrentBalance.innerHTML = Balance/100;
			Upgrade_4_Percentage.innerHTML = UpgradePercentage4;
			Upgrade_4_Owned.innerHTML = UpgradesOwned4;
			Upgrade_4_Cost.innerHTML = UpgradeCost4/100;

			}
			else
			{
				alert("You do not have enough funds!");
			}
	};
		UPGRADE_5.onclick = function()
		{

		if (Balance >= UpgradeCost5){
			Balance = Balance - UpgradeCost5;

			UpgradePercentage5 = hotel.increaseBonus();
			Hotel.UpgradePercentage = UpgradePercentage5;
			
			UpgradesOwned5 = hotel.increaseUO();
			Hotel.UpgradesOwned = UpgradesOwned5;

			UpgradeCost5 = hotel.nextUpgrade();
			Hotel.UpgradeCost = UpgradeCost5;

			CurrentBalance.innerHTML = Balance/100;
			Upgrade_5_Percentage.innerHTML = UpgradePercentage5;
			Upgrade_5_Owned.innerHTML = UpgradesOwned5;
			Upgrade_5_Cost.innerHTML = UpgradeCost5/100;

			}
			else
			{
				alert("You do not have enough funds!");
			}
	};
		
	//Stock table visibility toggle button
	TOGGLESTOCKTABLE.onclick = function()
	{
		if (StockTableVisible == false){
  			document.getElementById("StockTable").style.display = "block";
  			StockTableVisible = true;
  		}
  		else
  		{
  			document.getElementById("StockTable").style.display = "none";
  			StockTableVisible = false;
  		}
	};

	//Stock trading buttons
	//Buying stocks
	STOCKBUY_1.onclick = function()
	{
		NewShare = stock1.BuyPriceUpdate();
		StockShares1.innerHTML = NewShare;
		StockValue1.innerHTML = stock1.SharesValUpdate();
	};

	STOCKBUY_2.onclick = function()
	{
		NewShare = stock2.BuyPriceUpdate();
		StockShares2.innerHTML = NewShare;
		StockValue2.innerHTML = stock2.SharesValUpdate();
	};

	STOCKBUY_3.onclick = function()
	{
		NewShare = stock3.BuyPriceUpdate();
		StockShares3.innerHTML = NewShare;
		StockValue3.innerHTML = stock3.SharesValUpdate();
	};

	//Selling stocks
	STOCKSELL_1.onclick = function(){
		ShareSell = stock1.SellPriceUpdate();
		StockShares1.innerHTML = ShareSell;
		StockValue1.innerHTML = stock1.SharesValUpdate();
	};

	STOCKSELL_2.onclick = function(){
		ShareSell = stock2.SellPriceUpdate();
		StockShares2.innerHTML = ShareSell;
		StockValue2.innerHTML = stock2.SharesValUpdate();
	};

	STOCKSELL_3.onclick = function(){
		ShareSell = stock3.SellPriceUpdate();
		StockShares3.innerHTML = ShareSell;
		StockValue3.innerHTML = stock3.SharesValUpdate();
	};
	
	//Calculating next week
	NextWeek.onclick = function()
	{	
		//Adding another week to weekcount (score)
		WeekCount += 1;
		NextWeekCalc (EventChance,MoneyEventChance,EventList,RestaurantModifier,FactoryModifier,CinemaModifier,AutomobileModifier,HotelModifier);
	};

}

//Checking for save data
var LoadType = sessionStorage.getItem("loadtype")
if (LoadType == "LOAD")
	//Loading data
	{	
		var Data = LoadPlayerData();
		LoadBaseVariables(Data);
		
	}
	else
		//Starting a new game
	{
		StartUp();
	}
BusinessModel();