<?php 

	session_start();

	//Connecting to my database (MYSQL included in XAMPP)
	$host = "localhost";
	$user = "root";
	$pass = "";
	$dbname = "usersdb";

	$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

	$Username = $_SESSION['logged_in'];


	//Deciding which save is being loaded
	try
	{
		$SaveDate = $_POST['Savename1'];
		if ($SaveDate == "")
		{
			throw new Exception();		
		}
	}
	catch (Exception $e)
	{
		try
		{
			$SaveDate = $_POST['Savename2'];
			if ($SaveDate == "")
			{
				throw new Exception();		
			}		
		}
		catch (Exception $e)
		{
			try
			{
				$SaveDate = $_POST['Savename3'];
				if ($SaveDate == "")
				{
					throw new Exception();		
				}
			}
			catch (Exception $e)
			{
				echo "No saves stored!";
			}
		}
	}

	//Fetching UserID
	$sql = "SELECT ID FROM `logins_tbl` WHERE Username = '$Username'";

	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc())
		{
			$UserID = $row["ID"];
			
		}
	}
	else
	{
		echo "User not found";
	}

	//Using UserID to fetch SaveData
	$sql = "SELECT SaveData FROM saves_tbl WHERE UserID = '$UserID' AND Date = '$SaveDate'";

	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc())
		{
			$LoadData = $row["SaveData"];
			$LoadData = json_encode($LoadData);
			$_SESSION['savedata'] = $LoadData;
			header ("location: simulation.php");
			
		}
	}
	else
	{
		echo "Search not found";
	}

	$conn->close();	
	
	?>