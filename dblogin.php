<?php

	//Connecting to my database (MYSQL included in XAMPP)
	$host = "localhost";
	$user = "root";
	$pass = "";
	$dbname = "usersdb";

	$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

	//Hash function

	define("MODULO", 2484326083);
	define("PRIME", 2058806609);

	function ServerHash($Password)
	{
		$AsciiTot = 0;
		
		for ($i = 0; $i < strlen($Password); $i++)
		{	
			$AsciiTot += ord($Password[$i]);

		}
		
		$AsciiTot = $AsciiTot * PRIME;

		$HashedValue = fmod($AsciiTot,MODULO);
		
		return $HashedValue;	
	}
	//Comparing entered details to the database

	$EnteredUsername = $_POST['usernamelogin'];
	$EnteredPassword = $_POST['passwordlogin'];

	//Searching database for values corresponding to the username entered
	$sql = "SELECT Password, Salt FROM `logins_tbl` WHERE Username = '$EnteredUsername'";
	
	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc())
		{
			$ServerPassword = $row["Password"];
			$ServerSalt = $row["Salt"];
		}

		//Comparing values
		$FinalPassword = $ServerSalt.$EnteredPassword;
		$FinalPassword = dechex(ServerHash($FinalPassword));

		if ($FinalPassword == $ServerPassword)
		{
			session_start();
			$SendUsername = $EnteredUsername;
			$_SESSION['logged_in'] = $SendUsername;
			header ("refresh:0; url=home.php");
		}
		else
		{
			echo "Incorrect password";
			header ("refresh:5; url=index.html");
		}

	}
	else
	{
		echo "Username not recognised";
		header ("refresh:5; url=index.html");
	}

	$conn->close();
?>