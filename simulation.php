<html>

<head>
	<style>
		ul 
		{
		  list-style-type: none;
		  margin: 0;
		  padding: 0;
		  overflow: hidden;
		  background-color: #333;
		}

		li 
		{
		  float: left;
		  border-right:1px solid #bbb;
		}

		li a 
		{
		  display: block;
		  color: #ff8c00;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		}

		li a:hover 
		{
		  background-color: #111;
		}

		.dropdown 
		{
		  float: right;
		  overflow: hidden;
		}

		.dropdown .UserTab {
		  font-size: 16px;  
		  border: none;
		  outline: none;
		  color: #ff8c00;
		  padding: 14px 16px;
		  background-color: inherit;
		  font-family: inherit;
		  margin: 0;
		}

		.dropdown:hover .UserTab {
		  background-color: #111;
		}

		.dropdown-content {
		  display: none;
		  position: absolute;
		  border-style: solid;
		  border-color: #333;
		  right: 0;
		  background-color: #222;
		  min-width: 160px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  z-index: 1;
		}

		.dropdown-content a {
		  float: none;
		  color: #ff8c00;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		  text-align: left;
		}

		.dropdown-content a:hover {
		  background-color: #333;
		}

		.dropdown:hover .dropdown-content {
		  display: block;
		}
		.EventTableHeader
		{
		  height: 35%;
		  width: 30%;
		  position: fixed;
		  z-index: 1;
		  top: 100;
		  overflow-x: hidden;
		  padding-top: 20px;
		  background:#121;
		  font-size:25px;
		  color:#ff8c00;
		  text-align:center;
		  border:solid;
		  border-color:#ff8c00;

		}

	#EventTable
		{
		  height: 30%;
		  width: 30%;
		  position: fixed;
		  z-index: 1;
		  top: 180;
		  overflow-x: hidden;
		  padding-top: 20px;
		  left:11;
		  background:#121;
		}

		#EventTable tr
		{
			color:#ff8c00;
			font-size:20;
			text-align:center;
			font:bold;
		}

		.endgamemodal 
		{
			display: none; /* Hidden by default */
			position: fixed; /* Stay in place */
			z-index: 1; /* Sit on top */
			padding-top: 100px; /* Location of the box */
			left: 0;
			top: 0;
			width: 100%; /* Full width */
			height: 100%; /* Full height */
			overflow: auto; /* Enable scroll if needed */
			background-color: rgb(0,0,0); /* Fallback color */
			background-color: rgba(0,0,0,0.5); /* Black w/ opacity */
			text-align:center;
		}

		.modalcontent 
		{
			background-color: #fefefe;
			margin: auto;
			padding: 20px;
			border: 1px solid #888;
			width: 80%;
		}

		.modaltitle
		{

		}

		.modalbutton
		{

		}


	</style>

	<title>Businessman Simulator v1.2</title>

</head>


<body style="background:#282828">

	<nav>
		<ul>
		  <li><a href="home.php">Home</a></li>
		  <div class="dropdown">
		    <button id="AccountTab" class="UserTab" name="usernamebox">Account 
		      <i class="fa fa-caret-down"></i>
		    </button>
		    <div class="dropdown-content">
		      <a id="Logout_btn" href="logout.php">Logout</a>
		    </div>
		  </div> 
		
		</ul>
	</nav>

	<?php
		session_start();

		echo "<var id='savetransfer' hidden>";
		echo $_SESSION['savedata'];
		echo "</var>";
	?>

	
	<!__General Overview__>

	<style>
		#NewBusiness, .BuyNewBusinessToggle{

		}

		#NewBusiness {
			display: block;
		}
	</style>
	
	<p style="color:#ff8c00">Business Report - Week <span id="CurrentWeek"></span> - Year <span id="CurrentYear"></span></p>
	
	<p><center><h1 style="color:#ff8c00">Balance: <span style="color:#33CC00">$</span><span id="CurrentBalance" style="color:#33CC00"></span></h1></center></p>
	
	<center><button id="NextWeek" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Next Week</button></center>
	
	<center><button class = "BuyNewBusinessToggle" id="NewBusiness" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Buy New Business - $<span id="NewBusinessCost"></span></button></center>


	<!__EventLog__>
	<h3 class="EventTableHeader">Event Log</h3>
	<table id="EventTable">
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
	</table>

	<!__Upgrades Overview__>

	<br>
	<br>
	<center>
		<h3 style="color:#ff8c00">Upgrades</h3>
		<div class="row">
			<div class="column">
				<button id="Upgrade_1" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Upgrade Restaurant - $<span id="Upgrade_1_Cost"></span></button>
			</div>

			<div class="column">
				<button id="Upgrade_2" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Upgrade Factory - $<span id="Upgrade_2_Cost"></span></button>
			</div>

			<div class="column">
				<button id="Upgrade_3" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Upgrade Cinema - $<span id="Upgrade_3_Cost"></span></button>
			</div>

			<div class="column">
				<button id="Upgrade_4" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Upgrade Automobile - $<span id="Upgrade_4_Cost"></span></button>
			</div>

			<div class="column">
				<button id="Upgrade_5" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Upgrade Hotel - $<span id="Upgrade_5_Cost"></span></button>
			</div>
		</div>
	</center>

	<!__Stock Market__>
	
	<center><h3 style="color:#ff8c00">Stock Market</h3></center>
	<style>
		#StockTable, .ToggleStkTbl {
		}

		#StockTable {
			display: none;
		}
	</style>

	<style>
		table, th, td {
		border: 1px solid #ff8c00
		}
	</style>
	
	<center><button class = "ToggleStkTbl" id = "ToggleStockTable_btn" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00;padding:10px 32px">Toggle Stock Table</button></center>
	
	<center><table id = "StockTable" style="color:#ff8c00;background:#282828;width:20%">
		<tr>
			<th>Company</th>
			<th>Buy</th>
			<th>Sell</th>
			<th>Shares</th>
			<th>Value of Shares</th>
		</tr>
		<tr>
			<td>Silver</td>
			<td><button id="StockBuy1_btn" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00">$<span id="StockBuy1_val"></span></button></td>
			<td><button id="StockSell1_btn" type="button" style="color:#ff8c00;background:#282828;border-color:#ff8c00">$<span id="StockSell1_val"></span></button></td>
			<td><span id="StockShares1"></span></td>
			<td><span id="StockValue1"></span></td>
		</tr>
		<tr>
			<td>Oil</td>
			<td><button id="StockBuy2_btn" type="button" style="color:#ff8c00;background: #282828;border-color:#ff8c00">$<span id="StockBuy2_val"></span></button></td>
			<td><button id="StockSell2_btn" type="button" style="color:#ff8c00;background:#282828;border-color: #ff8c00">$<span id="StockSell2_val"></span></button></td>
			<td><span id="StockShares2"></span></td>
			<td><span id="StockValue2"></span></td>
		</tr>
		<tr>
			<td>Gold</td>
			<td><button id="StockBuy3_btn" type="button" style="color:#ff8c00;background:#282828;border-color: #ff8c00">$<span id="StockBuy3_val"></span></button></td>
			<td><button id="StockSell3_btn" type="button" style="color:#ff8c00;background: #282828;border-color: #ff8c00">$<span id="StockSell3_val"></span></button></td>
			<td><span id="StockShares3"></span></td>
			<td><span id="StockValue3"></span></td>
		</tr>
	</table></center>

	<!__Player Data__>

	<center>
		<form id="SaveForm" action="dbsavedata.php" method="post">
			<br>
			<input type="hidden" id="SaveDataField" name="playersavedata" value=" ">
			<input type="hidden" id="SaveDataUser" name="usernamesave" value=" ">
			<input type="hidden" id="UserScore" name="finaluserscore" value="">
			<input type="submit" id="SaveData_btn" value="Save Player Data" style="color:white;background:green;border-color:#ff8c00;padding:10px 32px">
		</form>
	</center>
	
	<!__Business Overview__>

	<center style="color:#ff8c00"><h3>Business Assets Overview</h3>
	
		<p>Number of Business Types Owned: <span id = "NewBusinessTotal"></span></p>
		
		<p>Restaurant Business</p>
		
		<p>Upgrades: <span id="Upgrade_1_Percentage"></span>% - Upgrades owned: <span id = "Upgrade_1_Owned"></span></p>

		<p>Factory Business</p>

		<p>Upgrades: <span id="Upgrade_2_Percentage"></span>% - Upgrades owned: <span id = "Upgrade_2_Owned"></span></p>

		<p>Cinema Business</p>

		<p>Upgrades: <span id="Upgrade_3_Percentage"></span>% - Upgrades owned: <span id = "Upgrade_3_Owned"></span></p>

		<p>Automobile Business</p>

		<p>Upgrades: <span id="Upgrade_4_Percentage"></span>% - Upgrades owned: <span id = "Upgrade_4_Owned"></span></p>

		<p>Hotel Business</p>

		<p>Upgrades: <span id="Upgrade_5_Percentage"></span>% - Upgrades owned: <span id = "Upgrade_5_Owned"></span></p>

	</center>

	<!__End Simulation__>

	<section id="EndGamePopup" class="endgamemodal">
		<div class="modalcontent">
			<h2 class="modaltitle"> You finished the game! </h2>
			<p class="modalstats"> Weeks taken: <span id="WeeksTaken"></span></p>
			<br>
			<p>Don't delete this save if it's your high score!</p>

			<form id="ModalSaveForm" action="dbsavedata.php" method="post">
				<br>
				<input type="hidden" id="ModalSaveDataField" name="playersavedata" value=" ">
				<input type="hidden" id="ModalSaveDataUser" name="usernamesave" value=" ">
				<input type="hidden" id="FinalUserScore" name="finaluserscore" value="">
				<input type="submit" id="ModalSaveGame_btn" value="Save game">
				
			</form>

			<a href="home.php">Homepage</a>
		</div>
	</section>
	
	<script src="simulation_base_script.js"></script>
	
</body>




</html>