<html>
	<head>
		<style>

		body 
		{
			background-color: #282828;
		}

		ul 
		{
		  list-style-type: none;
		  margin: 0;
		  padding: 0;
		  overflow: hidden;
		  background-color: #333;
		}

		li 
		{
		  float: left;
		  border-right:1px solid #bbb;
		}

		li a 
		{
		  display: block;
		  color: #ff8c00;
		  text-align: center;
		  padding: 14px 16px;
		  text-decoration: none;
		}

		h1
		{
			margin: 0 auto;
			width:230px;
			color:#ff8c00;
			border-bottom:solid;
			border-color:#ff8c00;
		}

		h2
		{
			margin: 0 auto;
			width:230px;
			color:#ff8c00;
		}

		input
		{
			color:#ff8c00;
			background:#282828;
			border-color:#ff8c00;
			padding:10px 10px;
		}

		form 
		{ 
			margin: 0 auto; 
			width:200px;
		}
		</style>
	</head>

	<body>

		<nav>
			<ul>
			  <li><a href="index.html">Log in!</a></li>
			</ul>
		</nav>
		<br>
		<br>
		<h1>Password Reset</h1>
		<br>
		<br>
		<h2>Check your email for the reset code.</h2>
		<br>
		<br>
		<form name="PasswordResetForm"  action="dbupdatepassword.php" onsubmit="return NewPasswordValidationFunc()" method="post">
			<input type="text" name="code" value="" id="ServerCode" placeholder="code">
			<br>
			<br>
			<input type="password" name="newpassword" value="" id="NewPassword" placeholder=" new password">
			<br>
			<br>
			<input type="password" name="confirmnewpassword" value="" id="ConfirmNewPassword" placeholder="confirm new password">
			<br>
			<br>
			<input type="submit" value="Submit" class="changepassbtn" id="ChangePassBtn">	
		</form>

		<section>
			<?php
				//Connecting to my database (MYSQL included in XAMPP)
				$host = "localhost";
				$user = "root";
				$pass = "";
				$dbname = "usersdb";

				$conn = mysqli_connect($host, $user, $pass, $dbname) or die("Connection Failed!");

				$Email = $_POST['passwordresetemail'];

				session_start();
				$_SESSION['emailval'] = $Email;

				//Creating random 8 digit code
				define("NumString", "0123456789");

				function RandomiseCode(){
					$Code = "";

					for ($i = 0; $i < 8; $i++)
					{
						$RandNum = random_int(0,(strlen(NumString)-1));
						$ListNum = strval(NumString[$RandNum]);
						$Code = $Code.$ListNum;
					}
					return $Code;

				}

				//Email validation
				$sql = "SELECT Email FROM `logins_tbl` WHERE Email = '$Email'";
					
				$result = $conn->query($sql);
				
				if ($result->num_rows > 0)
				{
					while($row = $result->fetch_assoc())
					{
						//Randomising code
						$Code = RandomiseCode();
						$_SESSION['servercode'] = $Code;

						//Sending email
						$Subject = "Businessman Simulator Password Reset";
						$Message = "
						<html>
						<head>
						<title>Simulation Password Reset</title>
						</head>
						<body>
						<p>Dear User,</p>
						<p>It seems like you have requested to reset your password on businesssimulation.com. Use the reset code below to proceed:</p>
						<p>Reset Code:</p>
						<p>$Code</p>
						<p></p>
						<p>Not you? Ignore this email.</p>
						</body>
						</html>
						";
						$Headers = "MIME-Version: 1.0" . "\r\n";
						$Headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$Headers .= 'From: <businesssimulation.com>' . "\r\n";

						mail($Email,$Subject,$Message,$Headers);

					}
				}	
				else
				{
					header ("refresh:0 url=index.html");

				}


				$conn->close();

			?>
		</section>

		<script>
			//Checking passwords match
			var NewPasswordCheck = document.getElementById("NewPassword");
			var ConfirmNewPasswordCheck = document.getElementById("ConfirmNewPassword");
			//Constants for hash
			const MODULO = 2484326083;
			const PRIME = 2058806609;

			function ValidatePassword()
			{
				if(NewPasswordCheck.value != ConfirmNewPasswordCheck.value) 
			  	{
			    	ConfirmNewPasswordCheck.setCustomValidity("Passwords Don't Match");
			  	} 
			  	else
			  	{
			    	ConfirmNewPasswordCheck.setCustomValidity('');
			  	}
			}
			//Hash function
			function Hash(Password)
			{	
				var AsciiTot = 0;
				
				for (i in Password)
				{	
					if (typeof Password[i] == "string")
					{
					AsciiTot += Password[i].charCodeAt(0);
					}
					else if(typeof Password[i] == "number")
					{
						AsciiTot += Password[i];
					}
					else
					{
						alert("Value type error!");
					}
				}
				
				AsciiTot = AsciiTot * PRIME;
				
				AsciiTot = AsciiTot % MODULO;
				
				return AsciiTot;
			}
			//Validating new password
			function NewPasswordValidationFunc()
			{
				var EnteredCode = document.forms["PasswordResetForm"]["code"].value;
				var NewPasswordVal = document.forms["PasswordResetForm"]["newpassword"].value;

				if (EnteredCode == "")
				{
					return false;
				}
				else if (EnteredCode.length != 8)
				{
					return false;
				}
				else if (NewPasswordVal == "")
				{
					return false;
				}
				else
				{
					//Hashing password
					Hashed = Hash(NewPasswordVal);
					Hashed = Hashed.toString(16);
					document.getElementById("NewPassword").value = Hashed;
				}
			}

			NewPasswordCheck.onchange = ValidatePassword;
			ConfirmNewPasswordCheck.onkeyup = ValidatePassword;
		</script>

	</body>
</html>